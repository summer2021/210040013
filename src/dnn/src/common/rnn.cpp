#include "megdnn/oprs.h"
#include "src/common/utils.h"
#include "src/cuda/rnn/utils.h"
#include "src/common/rnn.h"

namespace megdnn {

// RNNBase
/*void RNNBaseForward::deduce_layout(const TensorLayout& input, const TensorLayoutArray& weight_ih,
                                   const TensorLayoutArray& states, const TensorLayoutArray& weight_hh,
                                   const TensorLayoutArray& bias, TensorLayout& output,
                                   TensorLayoutArray& states_new)
{
    size_t seq_len = input.shape[0];
    size_t batch_size = input.shape[1];
    size_t D = 1;
    if (param().bidirectional) D = 2;
    size_t hidden_size = weight_hh[0].shape[1];
    output = TensorLayout(TensorShape{seq_len, batch_size, D * hidden_size}, input.dtype);
    for (int i = 0; i < states.size(); ++i)
        states_new.push_back(states[i]);
}

void RNNBaseForward::check_exec(const TensorLayout& input, const TensorLayoutArray& weight_ih,
                                const TensorLayoutArray& hx, const TensorLayoutArray& weight_hh,
                                const TensorLayoutArray& bias, const TensorLayout& output,
                                const TensorLayoutArray& states_new,
                                size_t workspace_in_bytes)
{
    TensorLayout output_expected;
    TensorLayoutArray states_new_expected;
    deduce_layout(input, weight_ih, hx, weight_hh, bias, output_expected, states_new_expected);
    megdnn_assert_eq_layout(output_expected, output);
    for (int i = 0; i < states_new.size(); ++i)
        megdnn_assert_eq_layout(states_new_expected[i], states_new[i]);
    auto required_workspace_in_bytes = get_workspace_in_bytes(input, weight_ih, hx, weight_hh, bias, output, states_new);
    megdnn_assert(workspace_in_bytes >= required_workspace_in_bytes);
}*/
}


/*
namespace megdnn {
namespace rnn {
using Param = param::RNN;

size_t get_workspace_in_bytes(const TensorLayout& input,
                        	  const TensorLayoutArray& weight_ih,
                        	  const TensorLayoutArray& states,
                        	  const TensorLayoutArray& weight_hh,
                        	  const TensorLayoutArray& bias,
                        	  const TensorLayout& output,
                        	  const TensorLayoutArray& states_new,
                              const Param& param,
                              Handle* handle)
{
	size_t num_layers = param.num_layers;
	size_t D = param.bidirectional ? 2 : 1;
	size_t seq_len = input.shape[0];
	size_t batch_size = input.shape[1];
	size_t input_size = input.shape[2];
	size_t hidden_size = weight_ih[0].shape[1];
	TensorLayout cell_output_layout{TensorShape{batch_size, hidden_size}, states[0].dtype};
	TensorLayout cell_first_input_layout{TensorShape{batch_size, input_size}, input.dtype};
	TensorLayout cell_input_layout{TensorShape{batch_size, D * hidden_size}, input.dtype};
	TensorLayout direction_output_layout{TensorShape{seq_len, batch_size, hidden_size}, output.dtype};


	size_t workspace_in_bytes = 0;
	auto cell_opr = handle->create_operator<RNNCellForward>();
	// cell_opr->param().bias = param().bias;
	if (num_layers == 1)
		workspace_in_bytes += cell_opr->get_workspace_in_bytes(
			cell_first_input_layout, weight_ih[0],
			cell_output_layout, weight_hh[0], bias[0],
			cell_output_layout
		);
	else
		workspace_in_bytes += cell_opr->get_workspace_in_bytes(
			cell_input_layout, weight_ih[1],
			cell_output_layout, weight_hh[1], bias[1],
			cell_output_layout
		);

	auto concat_opr = handle->create_operator<ConcatForward>();
	concat_opr->param().axis = -1;
	workspace_in_bytes += concat_opr->get_workspace_in_bytes(
		{direction_output_layout, direction_output_layout}, output
	);
	return workspace_in_bytes;
}

   void exec(_megdnn_tensor_in input, _megdnn_in const TensorNDArray& weight_ih,
        	 _megdnn_in const TensorNDArray& states, _megdnn_in const TensorNDArray& weight_hh,
             _megdnn_in const TensorNDArray& bias, _megdnn_tensor_out output,
             _megdnn_out const TensorNDArray& states_new, 
             _megdnn_workspace workspace, const Param& param, Handle* handle)
{
	size_t num_layers = param.num_layers;
	size_t D = param.bidirectional ? 2 : 1;
	size_t seq_len = input.layout.shape[0];
	size_t batch_size = input.layout.shape[1];
	size_t input_size = input.layout.shape[2];
	size_t hidden_size = weight_ih[0].layout.shape[1];
	TensorLayout cell_output_layout{TensorShape{batch_size, hidden_size}, states[0].layout.dtype};
	TensorLayout cell_first_input_layout{TensorShape{batch_size, input_size}, input.layout.dtype};
	TensorLayout cell_input_layout{TensorShape{batch_size, D * hidden_size}, input.layout.dtype};
	TensorLayout direction_output_layout{TensorShape{seq_len, batch_size, hidden_size}, output.layout.dtype};

	auto cell_opr = handle->create_operator<RNNCellForward>();
	// cell_opr->param().bias = param().bias;

	auto concat_opr = handle->create_operator<ConcatForward>();
	concat_opr->param().axis = -1;

	// copy states to states_new
	memcpy(states_new[0].raw_ptr, states[0].raw_ptr, states[0].layout.span().dist_byte());

	// layer 1
	TensorNDArray layer_outputs;
	for (size_t d = 0; d < D; ++d) {
		size_t cell_idx = d;
		TensorND hx = TensorND{states_new[0].raw_ptr + cell_idx, cell_output_layout};
		TensorND direction_output_tensor{output.raw_ptr + d * direction_output_layout.span().dist_byte(),
									     direction_output_layout};
		for (size_t step = 0; step < seq_len; ++step) {
			TensorND step_input{input.raw_ptr + step * batch_size * input_size, cell_first_input_layout};
			TensorND step_output{direction_output_tensor.raw_ptr + step * batch_size * hidden_size, cell_output_layout};
			cell_opr->exec(step_input, weight_ih[cell_idx], hx, weight_hh[cell_idx], bias[cell_idx], step_output, workspace);
			// copy step_output to hx
			memcpy(hx.raw_ptr, step_output.raw_ptr, hx.layout.span().dist_byte());
		}
		layer_outputs.push_back(direction_output_tensor);
		concat_opr->exec(layer_outputs, output, workspace);
	}


	for (size_t layer = 1; layer < num_layers; ++layer) {
		TensorNDArray layer_outputs;

		for (size_t d = 0; d < D; ++d) {
			size_t cell_idx = layer * D + d;

			TensorND hx = TensorND{states_new[0].raw_ptr + cell_idx, cell_output_layout};
			TensorND direction_output_tensor{output.raw_ptr + d * direction_output_layout.span().dist_byte(),
											 direction_output_layout};

			for (size_t step = 0; step < seq_len; ++step) {
				TensorND step_input{output.raw_ptr + step * batch_size * D * hidden_size, cell_input_layout};
				TensorND step_output{direction_output_tensor.raw_ptr + step * batch_size * hidden_size, cell_output_layout};
				cell_opr->exec(step_input, weight_ih[cell_idx], hx, weight_hh[cell_idx], bias[cell_idx], step_output, workspace);
				// copy step_output to hx
				memcpy(hx.raw_ptr, step_output.raw_ptr, hx.layout.span().dist_byte());
			}
			layer_outputs.push_back(direction_output_tensor);

			concat_opr->exec(layer_outputs, output, workspace);
		}
	}
}


}
}
*/

namespace megdnn {

/*size_t get_reserve_size(Handle* handle, megdnn::RNNForward::Param& param, const TensorLayout& input) {
#if CUDNN_MAJOR >= 6
	auto holder = megdnn::cuda::rnn::get_RNNDescHolder_v6(handle, param, input);
	return holder.reserveSpace_size;
# else
	return 0;
#endif
}*/

void RNN::deduce_layout(const TensorLayout& input, const TensorLayout& hx,
                        const TensorLayout& flatten_weights, TensorLayout& output,
						TensorLayout& hy, TensorLayout& reserve_space)
{
	// input: [seq_len, batch_size, input_size]
	// hx: [D * num_layers, batch_size, hidden_size]
    size_t seq_len = input.shape[0];
    size_t batch_size = input.shape[1];
    size_t D = param().bidirectional ? 2 : 1;
    size_t hidden_size = hx.shape[2];
    output = TensorLayout(TensorShape{seq_len, batch_size, D * hidden_size}, input.dtype);
	hy = TensorLayout(hx);
	// reserve_space = {{get_reserve_size(this->handle(), param(), input)}, dtype::Byte()};
	reserve_space = {{get_reserve_size_in_bytes(input)}, dtype::Byte()};
}


void RNN::check_exec(const TensorLayout& input, const TensorLayout& hx,
                     const TensorLayout& flatten_weights, const TensorLayout& output,
                     const TensorLayout& hy, const TensorLayout& reserve_space,
					 size_t workspace_in_bytes)
{
    auto errmsg = [&]() {
        std::string msg;
        msg.append("input=");
        msg.append(input.to_string());
        msg.append(", hx=");
        msg.append(hx.to_string());
		msg.append(", hidden_size=");
		msg.append(std::to_string(param().hidden_size));
        msg.append(", num_layers=");
        msg.append(std::to_string(param().num_layers));
        msg.append(", bidirectional=");
        msg.append(std::to_string(param().bidirectional));
        return msg;
    };
    size_t D = param().bidirectional ? 2 : 1;
	size_t num_layers = param().num_layers;
#define ASSERT_BRIEF(_content) \
	megdnn_assert(_content, "%s", errmsg().c_str());

	ASSERT_BRIEF(hx.ndim == 3)
	ASSERT_BRIEF(hx.shape[0] == D * num_layers)
	ASSERT_BRIEF(hx.shape[1] == input.shape[1])	// batch_size
	ASSERT_BRIEF(hx.shape[2] == param().hidden_size)
#undef ASSERT_BRIEF
}

void RNNBackward::deduce_layout(const TensorLayout& x, const TensorLayout& y, const TensorLayout& hx,
                       			const TensorLayout& dy, const TensorLayout& dhy,
								const TensorLayout& flatten_weights,
								const TensorLayout& reserve_space,   
                       			TensorLayout& dx, TensorLayout& dhx, TensorLayout& dw)
{
	dx = x;
	dhx = hx;
	dw = flatten_weights;
}

void RNNBackward::check_exec(const TensorLayout& x, const TensorLayout& y, const TensorLayout& hx,
                    		const TensorLayout& dy, const TensorLayout& dhy,
							const TensorLayout& flatten_weights,
							const TensorLayout& reserve_space,
                    		const TensorLayout& dx, const TensorLayout& dhx,const TensorLayout& dw,
                    		size_t workspace_in_bytes)
{

}

}	// namespace megdnn