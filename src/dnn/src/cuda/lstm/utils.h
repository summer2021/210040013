#pragma once
#include "src/cuda/cudnn_wrapper.h"
#include "src/cuda/rnn/utils.h"

namespace megdnn {
namespace cuda {
namespace lstm {
    using megdnn::cuda::rnn::RNNForwardDescHolder_v6;
    RNNForwardDescHolder_v6 get_RNNDescHolder_v6(Handle* handle,
                                                 megdnn::LSTMForward::Param& _param,
                                                 const TensorLayout& input);
}   // rnn
}   // cuda
}   // megdnn