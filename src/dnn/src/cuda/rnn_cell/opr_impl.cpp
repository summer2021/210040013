/*************************************************************************
	> File Name: opr_impl.cpp
	> Author: kxz
	> Mail: 15068701650@163.com 
	> Created Time: Thu 29 Jul 2021 11:03:03 AM CST
 ************************************************************************/
#include "src/common/rnn_cell.h"
#include "src/cuda/rnn_cell/opr_impl.h"


namespace megdnn{
namespace cuda {
	size_t RNNCellImpl::get_workspace_in_bytes(const TensorLayout& input,
                                      		   const TensorLayout& weight_ih,
											   const TensorLayout& bias_ih,
                                      		   const TensorLayout& hx,
                                      		   const TensorLayout& weight_hh,
                                      		   const TensorLayout& bias_hh,
                                      		   const TensorLayout& dst)
	{
		return megdnn::rnn_cell::get_workspace_in_bytes(
			input, weight_ih, bias_hh, hx, weight_hh, bias_hh, dst, handle()
		);
	}

	void RNNCellImpl::exec(_megdnn_tensor_in input, _megdnn_tensor_in weight_ih,
						   _megdnn_tensor_in bias_ih, _megdnn_tensor_in hx,
						   _megdnn_tensor_in weight_hh, _megdnn_tensor_in bias_hh,
						   _megdnn_tensor_out dst, _megdnn_workspace workspace)
	{
		megdnn::rnn_cell::exec(
			input, weight_ih, bias_ih, hx, weight_hh, bias_hh, dst, workspace,
			param().nonlineMode, handle()
		);
	}
}

} // namespace megdnn