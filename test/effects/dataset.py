#!/usr/bin/python
# -*- coding:utf-8 -*-
import torch
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pad_sequence

import megengine as mge

class Vocab:
    def __init__(self, vocab_path):
        with open(vocab_path, 'r') as fin:
            self.idx2word = list(map(lambda x: x.strip(), fin.readlines()))
        self.word2idx = {}
        for i, w in enumerate(self.idx2word):
            self.word2idx[w] = i
        specials = ['<unk>', '<eos>', '<pad>']
        for sp in specials:
            self.word2idx[sp] = len(self.idx2word)
            self.idx2word.append(sp)
    
    def __len__(self):
        return len(self.idx2word)

    def unk_idx(self):
        return self.word2idx['<unk>']
    
    def pad_idx(self):
        return self.word2idx['<pad>']


class TorchDataset(Dataset):
    def __init__(self, vocab, file_path, test_mode=False):
        super().__init__()
        self.vocab = vocab
        self.test_mode = test_mode
        with open(file_path, 'r') as fin:
            lines = fin.readlines()
        lines = list(map(lambda l: l.strip().split('\t'), lines))
        self.labels, self.indexes = [], []
        if test_mode:
            lines = lines[1:]
            self.indexes = [int(l[0]) for l in lines]
        else:
            self.labels = [int(l[1]) for l in lines]
        self.sentences = [l[-1].split() for l in lines]
        self.sentences = [list(map(lambda w: self.vocab.word2idx.get(w, self.vocab.unk_idx()), sent))
                          for sent in self.sentences]
    
    def __getitem__(self, idx):
        if self.test_mode:
            return self.indexes[idx], self.sentences[idx]
        return self.labels[idx], self.sentences[idx]
    
    def __len__(self):
        return len(self.sentences)

    def pad_collate(self, batch):
        labels, sents, lens = [], [], []
        for label, sent in batch:
            labels.append(label)
            sents.append(torch.tensor(sent))
            lens.append(len(sent))
        labels = torch.tensor(labels).long()
        sents = pad_sequence(sents, batch_first=True, padding_value=self.vocab.pad_idx())
        sents = sents.long()
        res = {
            'sentences': sents,
            'lengths': torch.tensor(lens).long()

        }
        if self. test_mode:
            res['indexes'] = labels
        else:
            res['labels'] = labels
        return res


class MgeDataset(mge.data.dataset.Dataset):
    def __init__(self, vocab, file_path, test_mode=False):
        super().__init__()
        self.vocab = vocab
        self.test_mode = test_mode
        with open(file_path, 'r') as fin:
            lines = fin.readlines()
        lines = list(map(lambda l: l.strip().split('\t'), lines))
        self.labels, self.indexes = [], []
        if test_mode:
            lines = lines[1:]
            self.indexes = [int(l[0]) for l in lines]
        else:
            self.labels = [int(l[1]) for l in lines]
        self.sentences = [l[-1].split() for l in lines]
        self.sentences = [list(map(lambda w: self.vocab.word2idx.get(w, self.vocab.unk_idx()), sent))
                          for sent in self.sentences]
    
    def __getitem__(self, idx):
        if self.test_mode:
            return self.indexes[idx], self.sentences[idx]
        return self.labels[idx], self.sentences[idx]
    
    def __len__(self):
        return len(self.sentences)

    def apply(self, batch):
        labels, sents, lens = [], [], []
        for label, sent in batch:
            labels.append(label)
            sents.append(sent)
            lens.append(len(sent))
        labels = mge.tensor(labels)
        max_len, pad_idx = max(lens), self.vocab.pad_idx()
        for sent in sents:
            for _ in range(len(sent), max_len):
                sent.append(pad_idx)
        sents = mge.tensor(sents)
        res = {
            'sentences': sents,
            'lengths': mge.tensor(lens)

        }
        if self. test_mode:
            res['indexes'] = labels
        else:
            res['labels'] = labels
        return res