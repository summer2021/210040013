#!/usr/bin/python
# -*- coding:utf-8 -*-
import os
import time
from tqdm import tqdm
import megengine as mge
import megengine.module as M
import megengine.functional as F
from megengine.autodiff import GradManager
from megengine.data import DataLoader, RandomSampler, SequentialSampler
from torch.utils.tensorboard import SummaryWriter   


GRAD_CLIP = 5.0


class RNNClassifier(M.Module):
    def __init__(self, model_type, **kwargs):
        super().__init__()
        vocab_len = kwargs.pop('vocab_len')
        self.embedding = M.Embedding(vocab_len, kwargs.get('input_size'))
        if model_type == 'lstm':
            self.rnn = M.LSTM(**kwargs)
        elif model_type == 'rnn_relu':
            self.rnn = M.RNN(nonlinearity='relu', **kwargs)
        elif model_type == 'rnn_tanh':
            self.rnn = M.RNN(nonlinearity='tanh', **kwargs)
        if kwargs.get('bidirectional'):
            D = 2
        else:
            D = 1
        self.cls = M.Linear(kwargs.get('hidden_size') * D, 1)
    
    def forward(self, batch):
        labels = batch['labels']
        sents = batch['sentences']
        lens = batch['lengths']
        embed = self.embedding(sents)
        output, _ = self.rnn(embed) # [bs, lens, hidden_size]
        output = output[F.arange(0, output.shape[0], dtype='int32'), lens - 1]
        output = F.tanh(output)
        res = self.cls(output)
        res = F.squeeze(res)
        res = F.sigmoid(res)
        loss = F.nn.binary_cross_entropy(res, labels, with_logits=False)
        pred_hit = ((res > 0.5) == labels).sum()
        # print(pred_hit.detach().item())
        # print('here')
        accu = pred_hit.detach().item() / len(labels)
        return loss, accu


def train(model_config, training_config, train_set, valid_set, model_type):

    print('='*6 + f'{model_type}_config' + '=' * 6)
    print(model_config)
    print('='*6 + 'trainning_config' + '='*6)
    print(training_config)
    epoch = training_config['epoch']
    gpu = training_config['gpu']
    mge.set_default_device(f'gpu{gpu}' if gpu != -1 else 'cpux')
    model = RNNClassifier(model_type, **model_config)
    gm = GradManager().attach(model.parameters())
    optimizer = mge.optimizer.Adam(model.parameters(), lr=training_config['lr'])
    model.train()
    patience = 3
    minloss = 10000
    early_stop = False
    step = 0
    writer = get_tensorboard(model_type)

    st = time.time()
    cal_elapsed = 0
    for e in range(epoch):
        if early_stop:
            break
        train_sampler = RandomSampler(train_set, batch_size=training_config['batch_size'])
        train_loader = DataLoader(train_set, train_sampler, collator=train_set)
        t = tqdm(train_loader, desc=f'epoch {e}')
        for batch in t:
            step += 1

            with gm:
                cal_start = time.time()
                loss, accu = model(batch)
                cal_elapsed += time.time() - cal_start

                gm.backward(loss)
                mge.optimizer.clip_grad_norm(model.parameters(), GRAD_CLIP)
                optimizer.step().clear_grad()
            writer.add_scalar('train_loss', loss.item(), step, time.time() - st)
            writer.add_scalar('train_accu', accu, step, time.time() - st)
            t.set_description(f'epoch: {e}, loss: {round(loss.item(), 2)}')
            
            # check_point
            if step % training_config['check_interval'] == 0:
                valid_sampler = SequentialSampler(valid_set, training_config['batch_size'])
                valid_loader = DataLoader(valid_set, valid_sampler, collator=valid_set)
                model.eval()
                val_losses, correct = [], 0
                for val_batch in tqdm(valid_loader):
                    val_loss, val_accu = model(val_batch)
                    val_losses.append(val_loss.item())
                    cur_batch_size = len(val_batch['lengths'])
                    correct += cur_batch_size * val_accu
                val_loss = sum(val_losses) / len(val_losses)
                val_accu = correct / len(valid_set)
                writer.add_scalar('val_loss', val_loss, step, time.time() - st)
                writer.add_scalar('val_accu', val_accu, step, time.time() - st)
                model.train()
                if val_loss < minloss:
                    patience = 3
                    # torch.save(model, training_config['save_path'])
                    minloss = val_loss
                    best_val_accu = val_accu
                    time_stamp = f'epoch {e}, global step {step}'
                else:
                    patience -= 1
                    if patience == 0:
                        early_stop = True
                        break

    elapsed = time.time() - st 
    print(f'elapsed {elapsed} s. calculation {cal_elapsed} s, average {cal_elapsed / step} s/step')
    print(f'val accu: {round(best_val_accu, 4)}, saved at {time_stamp}')


def get_tensorboard(model_type):
    pwd = os.path.abspath(__file__)
    pwd = os.path.split(pwd)[0]
    path = os.path.join(pwd, 'logs')
    path = os.path.join(path, f'mge_{model_type}')
    return SummaryWriter(path)