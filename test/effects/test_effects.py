#!/usr/bin/python
# -*- coding:utf-8 -*-
import sys
import argparse
from tqdm import tqdm
import dataset

def parse():
    test_mode = '--test' in sys.argv
    train_mode = not test_mode
    parser = argparse.ArgumentParser(description='test effects on CoLA')
    # training mode
    parser.add_argument('--train_set', type=str, required=train_mode,
                        help='tsv of training set')
    parser.add_argument('--valid_set', type=str, required=train_mode,
                        help='tsv of valid set')
    parser.add_argument('--save_path', type=str, required=train_mode,
                        help='path to save the model')
    # hyper-parameters
    parser.add_argument('--lr', type=float, required=train_mode,
                        help='learning rate')
    parser.add_argument('--epoch', type=int, required=train_mode,
                        help='max epoch to train')
    parser.add_argument('--check_interval', type=int, default=100,
                        help='interval of validation (steps)')
    parser.add_argument('--batch_size', type=int, default=32)
    parser.add_argument('--embed_size', type=int, default=300)
    parser.add_argument('--hidden_size', type=int, default=256)
    parser.add_argument('--num_layers', type=int, default=1)
    parser.add_argument('--bidirectional', action='store_true')

    # test mode
    parser.add_argument('--test', action='store_true')
    parser.add_argument('--test_set', type=str, required=test_mode,
                        help='tsv of test set')
    parser.add_argument('--ckpt', type=str, required=test_mode,
                        help='checkpoint to load')
    parser.add_argument('--output', type=str, required=test_mode,
                        help='path to save output results')

    # common
    parser.add_argument('--frame', type=str, choices=['torch', 'mge'],
                        required=train_mode or test_mode,
                        help='frame work to use')
    parser.add_argument('--vocab', type=str, required=True,
                        help='path to the vocabulary')
    parser.add_argument('--model', type=str, choices=['rnn_relu', 'rnn_tanh', 'lstm'],
                        required=train_mode or test_mode,
                        help='type of model')
    parser.add_argument('--gpu', type=int, required=True,
                        help='-1 for cpu')
    return parser.parse_args()


def get_model_config(args, vocab_len):
    return {
        'vocab_len': vocab_len,
        'input_size': args.embed_size,
        'hidden_size': args.hidden_size,
        'num_layers': args.num_layers,
        'batch_first': True,
        'bidirectional': args.bidirectional
    }


def get_training_config(args):
    return {
        'lr': args.lr,
        'epoch': args.epoch,
        'batch_size': args.batch_size,
        'check_interval': args.check_interval,
        'save_path': args.save_path,
        'gpu': args.gpu
    }

# torch
import torch_ver
def torch_train(args):
    vocab = dataset.Vocab(args.vocab)
    train_set = dataset.TorchDataset(vocab, args.train_set)
    valid_set = dataset.TorchDataset(vocab, args.valid_set)
    model_config = get_model_config(args, len(vocab))
    training_config = get_training_config(args)
    torch_ver.train(model_config, training_config, train_set, valid_set, args.model)


def torch_test(args):
    return


# mge
import mge_ver
def mge_train(args):
    vocab = dataset.Vocab(args.vocab)
    train_set = dataset.MgeDataset(vocab, args.train_set)
    valid_set = dataset.MgeDataset(vocab, args.valid_set)
    model_config = get_model_config(args, len(vocab))
    training_config = get_training_config(args)
    mge_ver.train(model_config, training_config, train_set, valid_set, args.model)
    return


def mge_test(args):
    return

if __name__ == '__main__':
    args = parse()
    if args.frame == 'torch':
        if args.test:
            torch_test(args)
        else:
            torch_train(args)
    elif args.frame == 'mge':
        if args.test:
            mge_test(args)
        else:
            mge_train(args)
    else:
        raise ValueError('framework not recognized')