#!/usr/bin/python
# -*- coding:utf-8 -*-
import os
import sys
import time
from tqdm import tqdm
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter   


GRAD_CLIP = 5.0
DEVICE = None


class LSTMClassifier(nn.Module):
    def __init__(self, **kwargs):
        super().__init__()
        vocab_len = kwargs.pop('vocab_len')
        self.embedding = nn.Embedding(vocab_len, kwargs.get('input_size'))
        self.lstm = nn.LSTM(**kwargs)
        if kwargs.get('bidirectional'):
            D = 2
        else:
            D = 1
        self.cls = nn.Sequential(
            nn.Tanh(),
            nn.Linear(kwargs.get('hidden_size') * D, 1),  # binary
            nn.Sigmoid()
        )
        self.loss = nn.BCELoss()

    
    def forward(self, batch):
        labels = batch['labels'].to(DEVICE)
        sents = batch['sentences'].to(DEVICE)
        lens = batch['lengths'].to(DEVICE)
        embed = self.embedding(sents)
        output, _ = self.lstm(embed) # [bs, lens, hidden_size]
        output = output[torch.arange(0, output.shape[0], device=output.device), lens - 1]
        res = self.cls(output).squeeze()
        loss = self.loss(res, labels.float())
        pred_hit = ((res > 0.5) == labels).sum()
        accu = pred_hit.detach().item() / len(labels)
        return loss, accu


def train(model_config, training_config, train_set, valid_set, model_type):
    global DEVICE

    print('='*6 + f'{model_type}_config' + '=' * 6)
    print(model_config)
    print('='*6 + 'trainning_config' + '='*6)
    print(training_config)
    epoch = training_config['epoch']
    gpu = training_config['gpu']
    DEVICE = torch.device(f'cuda:{gpu}' if gpu != -1 else 'cpu')
    if model_type == 'lstm':
        model = LSTMClassifier(**model_config)
    model.to(DEVICE)
    optimizer = torch.optim.Adam(model.parameters(), lr=training_config['lr'])
    model.train()
    patience = 3
    minloss = 10000
    early_stop = False
    step = 0
    writer = get_tensorboard(model_type)

    st = time.time()
    cal_elapsed = 0
    for e in range(epoch):
        if early_stop:
            break
        train_loader = DataLoader(train_set, training_config['batch_size'],
                                  shuffle=True, collate_fn=train_set.pad_collate)
        t = tqdm(train_loader, desc=f'epoch {e}')
        for batch in t:
            step += 1

            cal_start = time.time()
            loss, accu = model(batch)
            cal_elapsed += time.time() - cal_start

            optimizer.zero_grad()
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), GRAD_CLIP)
            optimizer.step()
            writer.add_scalar('train_loss', loss, step, time.time() - st)
            writer.add_scalar('train_accu', accu, step, time.time() - st)
            t.set_description(f'epoch: {e}, loss: {round(loss.item(), 2)}')
            
            # check_point
            if step % training_config['check_interval'] == 0:
                valid_loader = DataLoader(valid_set, training_config['batch_size'],
                                          collate_fn=valid_set.pad_collate)
                model.eval()
                val_losses, correct = [], 0
                for val_batch in tqdm(valid_loader):
                    val_loss, val_accu = model(val_batch)
                    val_losses.append(val_loss.item())
                    cur_batch_size = len(val_batch['lengths'])
                    correct += cur_batch_size * val_accu
                val_loss = sum(val_losses) / len(val_losses)
                val_accu = correct / len(valid_set)
                writer.add_scalar('val_loss', val_loss, step, time.time() - st)
                writer.add_scalar('val_accu', val_accu, step, time.time() - st)
                model.train()
                if val_loss < minloss:
                    patience = 3
                    torch.save(model, training_config['save_path'])
                    minloss = val_loss
                    best_val_accu = val_accu
                    time_stamp = f'epoch {e}, global step {step}'
                else:
                    patience -= 1
                    if patience == 0:
                        early_stop = True
                        break

    elapsed = time.time() - st 
    print(f'elapsed {elapsed} s. calculation {cal_elapsed} s, average {cal_elapsed / step} s/step')
    print(f'val accu: {round(best_val_accu, 4)}, saved at {time_stamp}')


def get_tensorboard(model_type):
    pwd = os.path.abspath(__file__)
    pwd = os.path.split(pwd)[0]
    path = os.path.join(pwd, 'logs')
    path = os.path.join(path, model_type)
    return SummaryWriter(path)