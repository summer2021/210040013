#!/bin/zsh
##########################################################################
# File Name: ../scripts/run_cola.sh
# Author: kxz
# mail: 15068701650@163.com
# Created Time: Sun 18 Jul 2021 04:03:46 PM CST
#########################################################################
echo "Use GPU: [$1]"
DATA_DIR=/data/disk3/private/kxz/CoLA
MODEL=lstm
if [ $2 ]; then
	MODEL=$2
fi
FRAME=torch

cd ../effects
python test_effects.py \
    --train_set ${DATA_DIR}/train.tsv \
    --valid_set ${DATA_DIR}/dev.tsv \
    --vocab ${DATA_DIR}/vocab.txt \
    --save_path ${DATA_DIR}/${FRAME}_${MODEL}.ckpt \
    --model ${MODEL} \
    --lr 1e-3 \
    --batch_size 32 \
    --epoch 3 \
    --frame $FRAME \
	--num_layers 2 \
	--bidirectional \
    --gpu $1
