#!/bin/zsh
##########################################################################
# File Name: run_test.sh
# Author: kxz
# mail: 15068701650@163.com
# Created Time: Fri 09 Jul 2021 11:33:53 AM CST
#########################################################################
DEVICE=0

if [ $1 ]; then
    DEVICE=$1
fi
FIGDIR=figs_${DEVICE}

TYPES=('rnn_relu' 'rnn_tanh' 'lstm')
IDXS=(0 1 2 3 4)
VARS=('input_size' 'hidden_size' 'seq_len' 'num_layers' 'batch_size')
MINS=(10 16 5 1 8)
MAXS=(300 512 100 10 2048)
STEPS=(10 16 5 1 8)

# cell
for type in ${TYPES[*]}
do
    for i in ${IDXS[*]}
    do
        echo "type ${type}, cell-only, var ${VARS[i]}"
        python ../speed_test.py \
            --output_dir ${FIGDIR} \
            --cell \
            --type ${type} \
            --var ${VARS[i]} \
            --min ${MINS[i]} \
            --max ${MAXS[i]} \
            --step ${STEPS[i]} \
            --device ${DEVICE}
    done
done

# not cell
for type in ${TYPES[*]}
do
    for i in ${IDXS[*]}
    do
        echo "type ${type}, var ${VARS[i]}"
        python ../speed_test.py \
            --output_dir ${FIGDIR} \
            --type ${type} \
            --var ${VARS[i]} \
            --min ${MINS[i]} \
            --max ${MAXS[i]} \
            --step ${STEPS[i]} \
            --device ${DEVICE}
    done
done
