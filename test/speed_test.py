#!/usr/bin/python
# -*- coding:utf-8 -*-
import argparse
import os
import time
from torch import functional
from tqdm import tqdm
import random
import numpy as np
import matplotlib.pyplot as plt
import megengine as mge
import megengine.module as M
import torch
import torch.nn as nn


def parse():
    parser = argparse.ArgumentParser(description='Compare speed of meg and torch')
    parser.add_argument('--repeat', type=int, default=10,
                        help='The number of times to repeat the same experiments')
    parser.add_argument('--output_dir', type=str, required=True,
                        help='Directory to save figures')
    parser.add_argument('--type', type=str, choices=['rnn_relu', 'rnn_tanh', 'lstm'], required=True,
                        help='Type of model to use')
    parser.add_argument('--cell', action='store_true', help='Test cells only')
    parser.add_argument('--var', type=str, choices=['input_size', 'hidden_size', 'seq_len', 'num_layers', 'batch_size'],
                        required=True, help='Variable in experiments')
    parser.add_argument('--bidirectional', action='store_true', help='Use bidirectional')
    parser.add_argument('--batch_first', action='store_true', help='Batch first')
    parser.add_argument('--min', type=int, required=True, help='Min value of the variable')
    parser.add_argument('--max', type=int, required=True, help='Max value of the variable')
    parser.add_argument('--step', type=int, required=True, help='Step of the variable')
    parser.add_argument('--device', type=int, default=-1, help='GPU id, -1 for cpu')
    return parser.parse_args()


def default_config(model_type, is_cell):
    config = {
        'input_size': 100,
        'hidden_size': 56,
    }
    if model_type == 'rnn_relu':
        config['nonlinearity'] = 'relu'
    elif model_type == 'rnn_tanh':
        config['nonlinearity'] = 'tanh'
    if not is_cell:
        config['num_layers'] = 1
    return config


def get_models(model_type, is_cell, config):
    nn_names = {name: i for i, name in enumerate(['RNNCell', 'LSTMCell', 'RNN', 'LSTM'])}
    nn_megs = [M.RNNCell, M.LSTMCell, M.RNN, M.LSTM]
    nn_torchs = [nn.RNNCell, nn.LSTMCell, nn.RNN, nn.LSTM]
    if model_type == 'lstm':
        name = 'LSTM'
    else:
        name = 'RNN'
    if is_cell:
        name += 'Cell'
    idx = nn_names[name]
    return nn_megs[idx](**config), nn_torchs[idx](**config)


def meg_run_once(model, model_config, run_config, repeat, is_cell):
    batch_size = run_config['batch_size']
    seq_len = run_config['seq_len']
    input_size = model_config['input_size']
    if is_cell:
        shape = (batch_size, input_size)
    else:
        if 'batch_first' in model_config:
            shape = (batch_size, seq_len, input_size)
        else:
            shape = (seq_len, batch_size, input_size)
    records = []
    for _ in range(repeat):
        x = mge.random.normal(size=shape)
        st = time.time()
        model(x)
        elapsed = time.time() - st
        records.append(elapsed)
    return sum(records)


def torch_run_once(model, model_config, run_config, repeat, is_cell, device):
    batch_size = run_config['batch_size']
    seq_len = run_config['seq_len']
    input_size = model_config['input_size']
    if is_cell:
        shape = (batch_size, input_size)
    else:
        if 'batch_first' in model_config:
            shape = (batch_size, seq_len, input_size)
        else:
            shape = (seq_len, batch_size, input_size)
    records = []
    for _ in range(repeat):
        x = torch.randn(*shape, device=device)
        st = time.time()
        model(x)
        elapsed = time.time() - st
        records.append(elapsed)
    return sum(records)


def main(args):
    if args.cell and (args.var == 'num_layers' or args.var == 'seq_len'):
        print(f'{args.var} is not changeable for cell, skip')
        return
    config = default_config(args.type, args.cell)
    if args.bidirectional:
        assert not args.cell
        config['bidirectional'] = True
    if args.batch_first:
        config['batch_first'] = True
    run_config = {
        'batch_size': 1024,
        'seq_len': 20
    }
    if args.device == -1:
        torch_device, meg_device = 'cpu', 'cpux'
    else:
        torch_device, meg_device = f'cuda:{args.device}', f'gpu{args.device}'
    mge.device.set_default_device(meg_device)
    meg_times, torch_times, vals = [], [], []
    var_name = args.var
    for val in tqdm(range(args.min, args.max, args.step)):
        if var_name in config:
            config[var_name] = val
        else:
            run_config[var_name] = val
        meg_model, torch_model = get_models(args.type, args.cell, config)
        torch_model.to(torch_device)
        if random.random() < 0.5:  # meg go first
            meg_times.append(meg_run_once(meg_model, config, run_config, args.repeat, args.cell))
            torch_times.append(torch_run_once(torch_model, config, run_config, args.repeat, args.cell, torch_device))
        else:
            torch_times.append(torch_run_once(torch_model, config, run_config, args.repeat, args.cell, torch_device))
            meg_times.append(meg_run_once(meg_model, config, run_config, args.repeat, args.cell))
        vals.append(val)
    plt.plot(vals, meg_times, label='megengine')
    plt.plot(vals, torch_times, label='pytorch')
    plt.xlabel(var_name)
    plt.ylabel('time(s)')
    plt.legend()

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    model_name = args.type
    if args.cell:
        model_name += '_cell'
    fig_name = os.path.join(args.output_dir, f'{model_name}_{var_name}_{meg_device}')
    plt.savefig(fig_name)


if __name__ == '__main__':
    main(parse())